#pragma once
#include "OutStream.h"
#include <fstream>
namespace msl {
	class FileStream : public OutStream

	{
	public:
		FileStream(char *path);
		~FileStream();
	};
}